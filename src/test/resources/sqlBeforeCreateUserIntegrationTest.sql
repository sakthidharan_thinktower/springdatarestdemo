create table master (
  uuid binary(16) NOT NULL,
  name varchar(50) NOT NULL
) engine=InnoDB default charset=utf8;

alter table master 
	ADD PRIMARY KEY (uuid),
	ADD UNIQUE (name);

create table master_details (
  uuid binary(16) NOT NULL,
  master_uuid binary(16) NOT NULL,
  name varchar(50) NOT NULL
) engine=InnoDB default charset=utf8;

alter table master_details
  add primary key (uuid),
  add key fk_masterid_idx (master_uuid),
  add constraint fk_masterid FOREIGN KEY (master_uuid) 
  	references master (uuid) ON DELETE NO ACTION ON UPDATE NO ACTION;
  
create table user (
  uuid binary(16) NOT NULL,
  name varchar(50) NOT NULL,
  type binary(16) NOT NULL,
  password varchar(128) NOT NULL,
  mobile_number varchar(20) NOT NULL,
  email varchar(100) NOT NULL,
  status boolean NOT NULL,
  created_date datetime NOT NULL,
  modified_date datetime,
  first_name varchar(50),
  last_name varchar(50),
  company_name varchar(50),
  created_by binary(16),
  modified_by binary(16)
) engine=InnoDB default charset=utf8;

alter table user
  add primary key (uuid),
  add key fk_usertype_idx (type);
  
alter table user  
  add constraint fk_usertype foreign key (type) references master_details (uuid),
  add constraint fk_created_by foreign key (created_by) references user (uuid),
  add constraint fk_modified_by foreign key (modified_by) references user (uuid);
  
insert into master (uuid,name) values (
	unhex(replace("12345678-1234-1234-1234-123456789abc","-","")),	"user");

insert into master_details (uuid,master_uuid,name) values (
	unhex(replace("12345678-1234-1234-1234-123456789000","-","")),
	unhex(replace("12345678-1234-1234-1234-123456789abc","-","")),"lead");
insert into master_details (uuid,master_uuid,name) values (
	unhex(replace("12345678-1234-1234-1234-123456789001","-","")),
	unhex(replace("12345678-1234-1234-1234-123456789abc","-","")),"customer");
insert into master_details (uuid,master_uuid,name) values (
	unhex(replace("12345678-1234-1234-1234-123456789002","-","")),
	unhex(replace("12345678-1234-1234-1234-123456789abc","-","")),"admin");