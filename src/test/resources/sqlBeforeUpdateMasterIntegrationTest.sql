CREATE TABLE master (
  uuid BINARY(16) NOT NULL,
  name varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE master 
	ADD PRIMARY KEY (uuid),
	ADD UNIQUE (name);
	
insert into master (uuid,name) values (
	unhex(replace("12345678-1234-1234-1234-123456789abc","-","")),	"user1");