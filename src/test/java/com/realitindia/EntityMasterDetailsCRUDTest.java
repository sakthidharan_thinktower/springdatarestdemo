package com.realitindia;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.text.MatchesPattern.matchesPattern;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.realitindia.model.EntityMasterDetailsRepository;
import com.realitindia.model.EntityMasterRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(
	webEnvironment = WebEnvironment.RANDOM_PORT,
	classes = DemoApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(
		  locations = "classpath:application-unittest.properties")
public class EntityMasterDetailsCRUDTest {
	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private EntityMasterRepository entityMasterRepository;
	
	@Autowired
	private EntityMasterDetailsRepository entityMasterDetailsRepository;

	@Autowired
    private ObjectMapper jsonCreator;    

	private String masterDetailsCRUDurl="/masterDetailsRecords";
	
	private String masterCRUDurl="/masterRecords";
	
	@Before
	public void deleteAllBeforeTests() throws Exception {
		entityMasterDetailsRepository.deleteAll();
		entityMasterRepository.deleteAll();
	}

//	@Ignore
	@Test
	public void shouldReturnRepositoryIndex() throws Exception {

		mockMvc.perform(get("/")).andDo(print()).andExpect(status().isOk()).andExpect(
				jsonPath("$._links.masterDetailsRecords").exists());
	}

//	@Ignore
	@Test
	public void shouldCreateEntity() throws Exception {

		ObjectNode masterNode=jsonCreator.createObjectNode();
		masterNode.put("name", "user");
		MvcResult mvcResult = mockMvc.perform(post(masterCRUDurl)
				.content(masterNode.toString()))
				.andExpect(status().isCreated()).andReturn();
		String masterLocation = mvcResult.getResponse().getHeader("Location");
		
		ObjectNode masterDetailsNode=jsonCreator.createObjectNode();
		masterDetailsNode.put("name", "lead");
		masterDetailsNode.put("master", masterLocation);
		mockMvc.perform(post(masterDetailsCRUDurl)
				.content(masterDetailsNode.toString()))
				.andExpect(status().isCreated())
				.andExpect(header().string("Location", containsString(masterDetailsCRUDurl)))
				.andExpect(header().string("Location", 
						matchesPattern(".*[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$")));		

	}

//	@Ignore
	@Test
	public void shouldRetrieveEntity() throws Exception {

		ObjectNode masterNode=jsonCreator.createObjectNode();
		masterNode.put("name", "user");
		MvcResult mvcResult = mockMvc.perform(post(masterCRUDurl)
				.content(masterNode.toString()))
				.andExpect(status().isCreated()).andReturn();
		String masterLocation = mvcResult.getResponse().getHeader("Location");

		ObjectNode masterDetailsNode=jsonCreator.createObjectNode();
		masterDetailsNode.put("name", "lead");
		masterDetailsNode.put("master", masterLocation);
		MvcResult mvcResult2 = mockMvc.perform(post(masterDetailsCRUDurl)
				.content(masterDetailsNode.toString()))
				.andExpect(status().isCreated()).andReturn();
		
		String masterDetailsLocation = mvcResult2.getResponse().getHeader("Location");
		
		mockMvc.perform(get(masterDetailsLocation)).andExpect(status().isOk())
		.andExpect(jsonPath("$.name").value("lead"))
		.andExpect(jsonPath("$._links.master.href").value(masterDetailsLocation+"/master"));

	}

//	@Ignore
	@Test
	public void shouldQueryEntity() throws Exception {

		ObjectNode masterNode=jsonCreator.createObjectNode();
		masterNode.put("name", "user");
		MvcResult mvcResult = mockMvc.perform(post(masterCRUDurl)
				.content(masterNode.toString()))
				.andExpect(status().isCreated()).andReturn();
		String masterLocation = mvcResult.getResponse().getHeader("Location");

		ObjectNode masterDetailsNode=jsonCreator.createObjectNode();
		masterDetailsNode.put("name", "lead");
		masterDetailsNode.put("master", masterLocation);
		mockMvc.perform(post(masterDetailsCRUDurl)
				.content(masterDetailsNode.toString()))
				.andExpect(status().isCreated());

		mockMvc.perform(
				get(masterDetailsCRUDurl+"/search/findByName?name={name}", "lead"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$._embedded.masterDetailsRecords[0].name").value("lead"));
	}

//	@Ignore
	@Test
	public void shouldUpdateEntity() throws Exception {
		ObjectNode masterNode=jsonCreator.createObjectNode();
		masterNode.put("name", "user");
		MvcResult mvcResultForCreateMaster = mockMvc.perform(post(masterCRUDurl)
				.content(masterNode.toString()))
				.andExpect(status().isCreated()).andReturn();
		String masterLocation = mvcResultForCreateMaster.getResponse().getHeader("Location");

		ObjectNode masterDetailsNode1=jsonCreator.createObjectNode();
		masterDetailsNode1.put("name", "lead1");
		masterDetailsNode1.put("master", masterLocation);
		MvcResult mvcResultForCreateMasterDetails = mockMvc.perform(post(masterDetailsCRUDurl)
				.content(masterDetailsNode1.toString()))
				.andExpect(status().isCreated())
				.andReturn();
		String masterDetailsLocation = mvcResultForCreateMasterDetails.getResponse().getHeader("Location");

		mockMvc.perform(get(masterDetailsLocation)).andExpect(status().isOk())
		.andExpect(jsonPath("$.name").value("lead1"));
		
		
		ObjectNode masterDetailsNode2=jsonCreator.createObjectNode();
		masterDetailsNode2.put("name", "lead2");
		masterDetailsNode2.put("master", masterLocation);
		mockMvc.perform(put(masterDetailsLocation).content(masterDetailsNode2.toString()))
			.andExpect(status().isNoContent());

		mockMvc.perform(get(masterDetailsLocation)).andExpect(status().isOk())
			.andExpect(jsonPath("$.name").value("lead2"));
	}

//	@Ignore
	@Test
	public void shouldPartiallyUpdateEntity() throws Exception {

		ObjectNode masterNode=jsonCreator.createObjectNode();
		masterNode.put("name", "user");
		MvcResult mvcResultForCreateMaster = mockMvc.perform(post(masterCRUDurl)
				.content(masterNode.toString()))
				.andExpect(status().isCreated()).andReturn();
		String masterLocation = mvcResultForCreateMaster.getResponse().getHeader("Location");

		ObjectNode masterDetailsNode1=jsonCreator.createObjectNode();
		masterDetailsNode1.put("name", "lead1");
		masterDetailsNode1.put("master", masterLocation);
		MvcResult mvcResultForCreateMasterDetails = mockMvc.perform(post(masterDetailsCRUDurl)
				.content(masterDetailsNode1.toString()))
				.andExpect(status().isCreated())
				.andReturn();
		String masterDetailsLocation = mvcResultForCreateMasterDetails.getResponse().getHeader("Location");

		mockMvc.perform(get(masterDetailsLocation)).andExpect(status().isOk())
		.andExpect(jsonPath("$.name").value("lead1"));

		mockMvc.perform(
				patch(masterDetailsLocation).content("{\"name\": \"lead2\"}"))
				.andExpect(status().isNoContent());

		mockMvc.perform(get(masterDetailsLocation))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.name").value("lead2"));
	}

//	@Ignore
	@Test
	public void shouldDeleteEntity() throws Exception {

		ObjectNode masterNode=jsonCreator.createObjectNode();
		masterNode.put("name", "user");
		MvcResult mvcResultForCreateMaster = mockMvc.perform(post(masterCRUDurl)
				.content(masterNode.toString()))
				.andExpect(status().isCreated()).andReturn();
		String masterLocation = mvcResultForCreateMaster.getResponse().getHeader("Location");

		ObjectNode masterDetailsNode1=jsonCreator.createObjectNode();
		masterDetailsNode1.put("name", "lead1");
		masterDetailsNode1.put("master", masterLocation);
		MvcResult mvcResultForCreateMasterDetails = mockMvc.perform(post(masterDetailsCRUDurl)
				.content(masterDetailsNode1.toString()))
				.andExpect(status().isCreated())
				.andReturn();
		String masterDetailsLocation = mvcResultForCreateMasterDetails.getResponse().getHeader("Location");

		mockMvc.perform(delete(masterDetailsLocation)).andExpect(status().isNoContent());

		mockMvc.perform(get(masterDetailsLocation)).andExpect(status().isNotFound());
	}
}
