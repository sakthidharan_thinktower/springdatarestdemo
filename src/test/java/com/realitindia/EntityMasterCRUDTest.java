/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.realitindia;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.realitindia.model.EntityMasterRepository;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.text.MatchesPattern.matchesPattern;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(
	webEnvironment = WebEnvironment.RANDOM_PORT,
	classes = DemoApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(
		  locations = "classpath:application-unittest.properties")
public class EntityMasterCRUDTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private EntityMasterRepository entityMasterRepository;

	@Autowired
    private ObjectMapper jsonCreator;    

	private String masterCRUDurl="/masterRecords";
	
	@Before
	public void deleteAllBeforeTests() throws Exception {
		entityMasterRepository.deleteAll();
	}

	
	
//	@Ignore
	@Test
	public void shouldReturnRepositoryIndex() throws Exception {

		mockMvc.perform(get("/")).andDo(print()).andExpect(status().isOk()).andExpect(
				jsonPath("$._links.masterRecords").exists());
	}

//	@Ignore
	@Test
	public void shouldCreateEntity() throws Exception {

		ObjectNode masterNode=jsonCreator.createObjectNode();
		masterNode.put("name", "user");
		
		mockMvc.perform(post(masterCRUDurl)
				.content(masterNode.toString()))
				.andExpect(status().isCreated())
				.andExpect(header().string("Location", containsString(masterCRUDurl)))
				.andExpect(header().string("Location", 
						matchesPattern(".*[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$")));		
	}

//	@Ignore
	@Test
	public void shouldRetrieveEntity() throws Exception {

		ObjectNode masterNode=jsonCreator.createObjectNode();
		masterNode.put("name", "user");
		
		MvcResult mvcResult = mockMvc.perform(post(masterCRUDurl)
				.content(masterNode.toString()))
				.andExpect(status().isCreated()).andReturn();

		String location = mvcResult.getResponse().getHeader("Location");
		mockMvc.perform(get(location)).andExpect(status().isOk())
			.andExpect(jsonPath("$.name").value("user"));
	}

//	@Ignore
	@Test
	public void shouldQueryEntity() throws Exception {

		ObjectNode masterNode=jsonCreator.createObjectNode();
		masterNode.put("name", "user");

		mockMvc.perform(post(masterCRUDurl).content(masterNode.toString()))
			.andExpect(status().isCreated());

		mockMvc.perform(
				get(masterCRUDurl+"/search/findByName?name={name}", "user"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$._embedded.masterRecords[0].name").value("user"));
	}

//	@Ignore
	@Test
	public void shouldUpdateEntity() throws Exception {

		ObjectNode masterNode=jsonCreator.createObjectNode();
		masterNode.put("name", "user1");
		
		MvcResult mvcResult = mockMvc.perform(post(masterCRUDurl).content(masterNode.toString()))
				.andExpect(status().isCreated())
				.andReturn();

		String location = mvcResult.getResponse().getHeader("Location");

		masterNode=jsonCreator.createObjectNode();

		masterNode.put("name", "user2");
		mockMvc.perform(put(location).content(masterNode.toString()))
			.andExpect(status().isNoContent());

		mockMvc.perform(get(location)).andExpect(status().isOk())
			.andExpect(jsonPath("$.name").value("user2"));
	}

//	@Ignore
	@Test
	public void shouldPartiallyUpdateEntity() throws Exception {

		ObjectNode masterNode=jsonCreator.createObjectNode();
		masterNode.put("name", "user1");
		
		MvcResult mvcResult = mockMvc.perform(post(masterCRUDurl).content(masterNode.toString()))
				.andExpect(status().isCreated())
				.andReturn();

		String location = mvcResult.getResponse().getHeader("Location");

		mockMvc.perform(
				patch(location).content("{\"name\": \"22user\"}")).andExpect(
						status().isNoContent());

		mockMvc.perform(get(location)).
				andExpect(status().isOk())
				.andExpect(jsonPath("$.name").value("22user"));
	}

//	@Ignore
	@Test
	public void shouldDeleteEntity() throws Exception {

		ObjectNode masterNode=jsonCreator.createObjectNode();
		masterNode.put("name", "user1");
		
		MvcResult mvcResult = mockMvc.perform(post(masterCRUDurl).content(masterNode.toString()))
				.andExpect(status().isCreated())
				.andReturn();

		String location = mvcResult.getResponse().getHeader("Location");

		mockMvc.perform(delete(location)).andExpect(status().isNoContent());

		mockMvc.perform(get(location)).andExpect(status().isNotFound());
	}
}