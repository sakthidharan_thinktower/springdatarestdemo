package com.realitindia.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="master_details")
public class EntityMasterDetails {

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2",strategy = "uuid2")	
	@Column(updatable = false, nullable = false, columnDefinition="BINARY(16)")
	private UUID uuid;

	@ManyToOne(optional=false)
	@JoinColumn(name="master_uuid", referencedColumnName="uuid", nullable=false)
	private EntityMaster master;

	@NotNull
	@Size(min=1, max=50)
	private String name;

	public EntityMaster getMaster() {
		return master;
	}

	public void setMaster(EntityMaster master) {
		this.master = master;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
