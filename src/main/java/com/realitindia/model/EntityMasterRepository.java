package com.realitindia.model;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "masterRecords", path = "masterRecords")
//public interface EntityMasterRepository extends PagingAndSortingRepository<EntityMaster, UUID> {
public interface EntityMasterRepository extends JpaRepository<EntityMaster, UUID> {	
	List<EntityMaster> findByName(@Param("name") String name);
}
