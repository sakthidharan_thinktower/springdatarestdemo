package com.realitindia.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="master",uniqueConstraints={@UniqueConstraint(columnNames={"name"})})
public class EntityMaster {
	
	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2",strategy = "uuid2")	
	@Column(updatable = false, nullable = false, columnDefinition="BINARY(16)")
	private UUID uuid;
	
	@Size(min=1, max=50)
	private String name;
	
	public String getName() {
		return name;
	}	
	public void setName(String name) {
		this.name=name;
	}
	
}
