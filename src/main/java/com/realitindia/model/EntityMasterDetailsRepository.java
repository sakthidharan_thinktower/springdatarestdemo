package com.realitindia.model;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "masterDetailsRecords", path = "masterDetailsRecords")
public interface EntityMasterDetailsRepository extends JpaRepository<EntityMasterDetails, UUID> {
	List<EntityMasterDetails> findByName(@Param("name") String name);
}